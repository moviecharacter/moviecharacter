package com.movie.character.MovieCharacterAPI;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;

@SpringBootApplication
@OpenAPIDefinition(info = @Info(title = "Movie Character API", version = "1.0", description = "Movie character API"))
public class MovieCharacterApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(MovieCharacterApiApplication.class, args);
	}

}
