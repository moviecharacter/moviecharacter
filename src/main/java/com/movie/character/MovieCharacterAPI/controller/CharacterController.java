package com.movie.character.MovieCharacterAPI.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.movie.character.MovieCharacterAPI.dto.character.CharacterDTO;
import com.movie.character.MovieCharacterAPI.service.CharacterService;

@RestController
@RequestMapping("/characters")
public class CharacterController {
    @Autowired
    CharacterService characterService;

    @GetMapping
    public ResponseEntity<List<CharacterDTO>> characters(){
        return new ResponseEntity<>(characterService.characters(), HttpStatus.OK);
    }

    @GetMapping("{id}")
    public ResponseEntity<CharacterDTO> character(@PathVariable("id") Integer id){
        return new ResponseEntity<>(characterService.character(id), HttpStatus.OK) ;
    }

    @PostMapping
    public ResponseEntity<CharacterDTO> addCharacter(@RequestBody CharacterDTO CharacterDTO){
        return new ResponseEntity<>(characterService.addCharacter(CharacterDTO), HttpStatus.OK);
    }

    @DeleteMapping("{id}")
    public ResponseEntity<String> deleteCharacter(@PathVariable("id") Integer id){
        return new ResponseEntity<>(characterService.deleteCharacter(id), HttpStatus.OK) ;
    }

    @PutMapping("{id}")
    public ResponseEntity<CharacterDTO> updateCharacter(@Valid @RequestBody CharacterDTO CharacterDTO, @PathVariable("id") Integer id){
        return new ResponseEntity<>(characterService.updateCharacter(CharacterDTO, id), HttpStatus.OK);
    }
}
