package com.movie.character.MovieCharacterAPI.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.movie.character.MovieCharacterAPI.dto.movie.MovieDTO;
import com.movie.character.MovieCharacterAPI.service.MovieService;

@RestController
@RequestMapping("/movies")
public class MovieController {
    @Autowired 
    MovieService movieService;

    @GetMapping
    public ResponseEntity<List<MovieDTO>> movies(){
        return new ResponseEntity<>(movieService.movies(), HttpStatus.OK);
    }

    @GetMapping("{id}")
    public ResponseEntity<MovieDTO> movie(@PathVariable("id") Integer id){
        return new ResponseEntity<>(movieService.movie(id), HttpStatus.OK) ;
    }

    @PostMapping
    public ResponseEntity<MovieDTO> addMovie(@RequestBody MovieDTO movieDTO){
        return new ResponseEntity<>(movieService.addMovie(movieDTO), HttpStatus.OK);
    }

    @DeleteMapping("{id}")
    public ResponseEntity<String> deleteMovie(@PathVariable("id") Integer id){
        return new ResponseEntity<>(movieService.deleteMovie(id), HttpStatus.OK) ;
    }

    @PutMapping("{id}")
    public ResponseEntity<MovieDTO> updateMovie(@RequestBody MovieDTO movieDTO, @PathVariable("id") Integer id){
        return new ResponseEntity<>(movieService.updateMovie(movieDTO, id), HttpStatus.OK);
    }
}
