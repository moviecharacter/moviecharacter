package com.movie.character.MovieCharacterAPI.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.movie.character.MovieCharacterAPI.dto.franchise.FranchiseDTO;
import com.movie.character.MovieCharacterAPI.service.FranchiseService;

@RestController
@RequestMapping("/franchises")
public class FranchiseController {
    @Autowired
    FranchiseService franchiseService;

    @GetMapping
    public ResponseEntity<List<FranchiseDTO>> franchises(){
        return new ResponseEntity<>(franchiseService.franchises(), HttpStatus.OK);
    }

    @GetMapping("{id}")
    public ResponseEntity<FranchiseDTO> getFranchise(@PathVariable("id") Integer id){
        return new ResponseEntity<>(franchiseService.franchise(id), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<FranchiseDTO> saveFranchise(@RequestBody FranchiseDTO franchiseDTO){
        return new ResponseEntity<>(franchiseService.addFranchise(franchiseDTO), HttpStatus.OK);        
    }

    @PutMapping("{id}")
    public ResponseEntity<FranchiseDTO> updateFranchise(@RequestBody FranchiseDTO franchiseDTO, @PathVariable("id") Integer franchiseId){
        return new ResponseEntity<>(franchiseService.updateFranchise(franchiseDTO, franchiseId), HttpStatus.OK);        
    }

    @DeleteMapping("{id}")
    public ResponseEntity<FranchiseDTO> updateFranchise(@PathVariable("id") Integer franchiseId){
        return new ResponseEntity<>(franchiseService.deleteFranchise(franchiseId), HttpStatus.OK);        
    }

}
