package com.movie.character.MovieCharacterAPI.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "character")
public class Character {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "full_name", nullable = false, length = 50)
    private String fullName;

    @Column(name = "alias", length = 50)
    private String alias;

    @Column(name = "gender", nullable = false, length = 10)
    private String gender;

    @Column(name = "picture", columnDefinition = "TEXT")
    private String picture;

    @ManyToMany(mappedBy = "characters")
    @JsonIgnore
    private Set<Movie> Movie; 

    public void addMovie(Movie movie) {
        this.Movie.add(movie);
        movie.getCharacters().add(this);
    }

    public void removeMovie(Movie movie) {
        this.getMovie().remove(movie);
        movie.getCharacters().remove(this);
    }

    public void removeMovies() {
        for (Movie movie : new HashSet<>(Movie)) {
            removeMovie(movie);
        }
    }
}
