package com.movie.character.MovieCharacterAPI.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "movie")
public class Movie {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "movie_title", length = 100)
    private String movieTitle;

    @Column(name = "genre", columnDefinition = "TEXT")
    private String genre;

    @Column(name = "release_year", length = 4)
    private String releaseYear;

    @Column(name = "director", length = 100)
    private String director;

    @Column(name = "picture", columnDefinition = "TEXT")
    private String picture;

    @Column(name = "trailer", columnDefinition = "TEXT")
    private String trailer;

    @ManyToOne
    @JoinColumn(name = "franchise_id")
    private Franchise franchise;

    @ManyToMany
    @JoinTable(name = "movie_character", 
    joinColumns = {
        @JoinColumn(name = "movie_id")
    },
    inverseJoinColumns = {
        @JoinColumn(name = "character_id")
    })
    private Set<Character> characters;

    public void addCharacter(Character character) {
        this.characters.add(character);
        character.getMovie().add(this);
    }
    public void removeCharacter(Character character) {
        this.getCharacters().remove(character);
        character.getMovie().remove(this);
    }
    public void removeCharacters() {
        for (Character character : new HashSet<>(characters)) {
            removeCharacter(character);
        }
    }

}
