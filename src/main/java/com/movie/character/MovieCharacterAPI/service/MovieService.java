package com.movie.character.MovieCharacterAPI.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.movie.character.MovieCharacterAPI.dto.movie.MovieDTO;

@Service
public interface MovieService {
    public List<MovieDTO> movies();

    public MovieDTO movie(Integer id);

    public MovieDTO addMovie(MovieDTO movieDTO);

    public MovieDTO updateMovie(MovieDTO movieDTO, Integer id);

    public String deleteMovie(Integer movieId);
}
