package com.movie.character.MovieCharacterAPI.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.movie.character.MovieCharacterAPI.dto.character.CharacterDTO;

@Service
public interface CharacterService {
    public List<CharacterDTO> characters();

    public CharacterDTO character(Integer id);

    public CharacterDTO addCharacter(CharacterDTO CharacterDTO);

    public CharacterDTO updateCharacter(CharacterDTO CharacterDTO, Integer characterId);

    public String deleteCharacter(Integer franchiseId);
}
