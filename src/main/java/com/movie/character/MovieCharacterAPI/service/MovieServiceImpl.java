package com.movie.character.MovieCharacterAPI.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.Resource;
import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.movie.character.MovieCharacterAPI.dto.movie.MovieDTO;
import com.movie.character.MovieCharacterAPI.model.Character;
import com.movie.character.MovieCharacterAPI.model.Movie;
import com.movie.character.MovieCharacterAPI.repository.CharacterRepository;
import com.movie.character.MovieCharacterAPI.repository.FranchiseRepository;
import com.movie.character.MovieCharacterAPI.repository.MovieRepository;

@Service
public class MovieServiceImpl implements MovieService{
    @Resource 
    MovieRepository movieRepository;

    @Resource
    CharacterRepository characterRepository;

    @Resource
    FranchiseRepository franchiseRepository;

    @Override
    public List<MovieDTO> movies() {
        List<MovieDTO> movieDTOs = new ArrayList<>();
        List<Movie> movies = movieRepository.findAll();
        movies.stream().forEach(movie -> {
        MovieDTO movieDTO = mapEntityToDto(movie);
            movieDTOs.add(movieDTO);
        });
        return movieDTOs;
    }

    @Override
    public MovieDTO movie(Integer id) {
        Movie movie = movieRepository.findById(id).get();
        MovieDTO movieDTO = mapEntityToDto(movie);
        return movieDTO;
    }

    @Transactional
    @Override
    public MovieDTO addMovie(MovieDTO movieDTO) {
        Movie movie = new Movie();
        mapDtoToEntity(movieDTO, movie);
        Movie savedMovie = movieRepository.save(movie);
        return mapEntityToDto(savedMovie);
    }

    @Transactional
    @Override
    public MovieDTO updateMovie(MovieDTO movieDTO, Integer id) {
        Movie movie = movieRepository.findById(id).get();
        movie.getCharacters().clear();
        mapDtoToEntity(movieDTO, movie);
        Movie movie2 = movieRepository.save(movie);
        return mapEntityToDto(movie2);
    }

    @Override
    public String deleteMovie(Integer movieId) {
        Optional<Movie> movie = movieRepository.findById(movieId);
        if(movie.isPresent()) {
            movie.get().removeCharacters();
            movieRepository.deleteById(movie.get().getId());
            return "Movie with id: " + movieId + " deleted successfully!";
        }        
        return null;
    }

    private MovieDTO mapEntityToDto(Movie movie) {
        MovieDTO responseDto = new MovieDTO();
        responseDto.setId(movie.getId());
        responseDto.setMovieTitle(movie.getMovieTitle());
        responseDto.setGenre(movie.getGenre());
        responseDto.setReleaseYear(movie.getReleaseYear());
        responseDto.setDirector(movie.getDirector());
        responseDto.setPicture(movie.getPicture());
        responseDto.setTrailer(movie.getTrailer());
        responseDto.setFranchise_id(movie.getFranchise().getId());
        responseDto.setCharacters(movie.getCharacters().stream().map(com.movie.character.MovieCharacterAPI.model.Character::getId).collect(Collectors.toSet()));
        return responseDto;
    }

    private void mapDtoToEntity(MovieDTO movieDTO, Movie movie) {
        movie.setMovieTitle(movieDTO.getMovieTitle());
        movie.setDirector(movieDTO.getDirector());
        movie.setGenre(movieDTO.getGenre());
        movie.setPicture(movieDTO.getPicture());
        movie.setReleaseYear(movieDTO.getReleaseYear());
        movie.setTrailer(movieDTO.getTrailer());
        if( movieDTO.getFranchise_id() != null ){
            movie.setFranchise(franchiseRepository.findById(movieDTO.getFranchise_id()).get());
        }
        if (null == movie.getCharacters()) {
            movie.setCharacters(new HashSet<>());
        }
        if(movieDTO.getCharacters() != null){
            movieDTO.getCharacters().stream().forEach(id -> {
                Character character = characterRepository.findById(id).get();
                if (null == character) {
                    character = new Character();
                    character.setMovie(new HashSet<>());
                }
                movie.addCharacter(character);
            });
        }
    }

}
