package com.movie.character.MovieCharacterAPI.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.movie.character.MovieCharacterAPI.dto.character.CharacterDTO;
import com.movie.character.MovieCharacterAPI.model.Character;
import com.movie.character.MovieCharacterAPI.model.Movie;
import com.movie.character.MovieCharacterAPI.repository.CharacterRepository;
import com.movie.character.MovieCharacterAPI.repository.MovieRepository;

@Service
public class CharacterServiceImpl implements CharacterService{
    @Resource
    CharacterRepository characterRepository;

    @Resource
    MovieRepository movieRepository;

    @Override
    public List<CharacterDTO> characters() {
        List<CharacterDTO> characterDTOs = new ArrayList<>();
        List<Character> characters = characterRepository.findAll();
        characters.stream().forEach(character -> {
            CharacterDTO characterDTO = mapEntityToDto(character);
            characterDTOs.add(characterDTO);
        });
        return characterDTOs;
    }

    @Override
    public CharacterDTO character(Integer id) {
        Character character = characterRepository.findById(id).get();
        CharacterDTO characterDTO = mapEntityToDto(character);
        return characterDTO;
    }

    @Override
    public CharacterDTO addCharacter(CharacterDTO CharacterDTO) {
        Character character = new Character();
        mapDtoToEntity(CharacterDTO, character);
        Character savedCharacter = characterRepository.save(character);
        return mapEntityToDto(savedCharacter);
    }

    @Override
    public CharacterDTO updateCharacter(CharacterDTO CharacterDTO, Integer characterId) {
        Character character = characterRepository.findById(characterId).get();
        character.getMovie().clear();
        mapDtoToEntity(CharacterDTO, character);
        Character character2 = characterRepository.save(character);
        return mapEntityToDto(character2);
    }

    @Override
    public String deleteCharacter(Integer characterId) {
        Optional<Character> character = characterRepository.findById(characterId);
        if(character.isPresent()) {
            character.get().removeMovies();;
            characterRepository.deleteById(character.get().getId());
            return "Character with id: " + characterId + " deleted successfully!";
        }        
        return null;
    }

    private CharacterDTO mapEntityToDto(Character character) {
        CharacterDTO responseDto = new CharacterDTO();
        responseDto.setId(character.getId());
        responseDto.setFullName(character.getFullName());
        responseDto.setAlias(character.getAlias());
        responseDto.setGender(character.getGender());
        responseDto.setPictures(character.getPicture());
        responseDto.setMovies(character.getMovie().stream().map(Movie::getId).collect(Collectors.toSet()));
        return responseDto;
    }

    private void mapDtoToEntity(CharacterDTO characterDTO, Character character) {
        character.setFullName(characterDTO.getFullName());
        character.setAlias(characterDTO.getAlias());
        character.setGender(characterDTO.getGender());
        character.setPicture(characterDTO.getPictures());

        if (null == character.getMovie()) {
            character.setMovie(new HashSet<>());
        }
        if(characterDTO.getMovies() != null){
            characterDTO.getMovies().stream().forEach(id -> {
                Movie movie = movieRepository.findById(id).get();
                if (null == movie) {
                    movie = new Movie();
                    movie.setCharacters(new HashSet<>());
                }
                character.addMovie(movie);
            });
        }
    }
    
}
