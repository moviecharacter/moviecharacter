package com.movie.character.MovieCharacterAPI.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.movie.character.MovieCharacterAPI.dto.franchise.FranchiseDTO;

@Service
public interface FranchiseService {
    public List<FranchiseDTO> franchises();

    public FranchiseDTO franchise(Integer id);

    public FranchiseDTO addFranchise(FranchiseDTO FranchiseDTO);

    public FranchiseDTO updateFranchise(FranchiseDTO FranchiseDTO, Integer franchiseId);

    public FranchiseDTO deleteFranchise(Integer franchiseId);
}
