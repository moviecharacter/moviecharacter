package com.movie.character.MovieCharacterAPI.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.movie.character.MovieCharacterAPI.dto.franchise.FranchiseDTO;
import com.movie.character.MovieCharacterAPI.model.Franchise;
import com.movie.character.MovieCharacterAPI.model.Movie;
import com.movie.character.MovieCharacterAPI.repository.FranchiseRepository;

@Service
public class FranchiseServiceImpl implements FranchiseService{
    @Resource
    FranchiseRepository franchiseRepository;

    @Override
    public List<FranchiseDTO> franchises() {
        List<FranchiseDTO> franchiseMovieDTOs = new ArrayList<>();
        List<Franchise> franchises = franchiseRepository.findAll();
        franchises.stream().forEach(franchise -> {
            FranchiseDTO franchiseDTO = mapEntityToDto(franchise);
            franchiseMovieDTOs.add(franchiseDTO);
        });
        return franchiseMovieDTOs;
    }

    @Override
    public FranchiseDTO franchise(Integer id) {
        Franchise franchise = franchiseRepository.findById(id).get();
        FranchiseDTO franchiseDTO = mapEntityToDto(franchise);
        return franchiseDTO;
    }

    @Override
    public FranchiseDTO addFranchise(FranchiseDTO FranchiseDTO) {
        Franchise franchise = mapDtoToEntity(FranchiseDTO);
        franchise = franchiseRepository.save(franchise);
        return mapEntityToDto(franchise);
    }

    @Override
    public FranchiseDTO updateFranchise(FranchiseDTO franchiseDTO, Integer franchiseId) {
        Franchise franchise = franchiseRepository.findById(franchiseId).get();
        franchise.setName(franchiseDTO.getName());
        franchise.setDescription(franchiseDTO.getDescription());
        FranchiseDTO franchiseDTO2 = mapEntityToDto(franchiseRepository.save(franchise));
        return franchiseDTO2;
    }

    @Override
    public FranchiseDTO deleteFranchise(Integer franchiseId) {
        Franchise franchise = franchiseRepository.findById(franchiseId).get();
        franchiseRepository.delete(franchise);
        return mapEntityToDto(franchise);
    }

    private FranchiseDTO mapEntityToDto(Franchise franchise){
        FranchiseDTO franchiseDTO = new FranchiseDTO();
        franchiseDTO.setId(franchise.getId());
        franchiseDTO.setName(franchise.getName());
        franchiseDTO.setDescription(franchise.getDescription());
        if( franchise.getMovies() != null ){
            franchiseDTO.setMovies(franchise.getMovies().stream().map(Movie::getId).collect(Collectors.toSet()));
        }
        return franchiseDTO;
    }

    private Franchise mapDtoToEntity(FranchiseDTO franchiseDTO){
        Franchise franchise = new Franchise();
        franchise.setName(franchiseDTO.getName());
        franchise.setDescription(franchiseDTO.getDescription());
        return franchise;
    }
    
}
