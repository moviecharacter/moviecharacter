package com.movie.character.MovieCharacterAPI.dto.franchise;

import java.util.Set;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FranchiseDTO {
    private Integer id;
    private String description;
    private String name;
    private Set<Integer> movies;
}
