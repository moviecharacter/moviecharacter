package com.movie.character.MovieCharacterAPI.dto.character;

import java.util.Set;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CharacterDTO {
    private Integer id;
    @NotNull
    private String fullName;
    private String alias;
    @NotNull
    private String gender;
    private String pictures;
    private Set<Integer> movies;
}
