package com.movie.character.MovieCharacterAPI.dto.movie;

import java.util.Set;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MovieDTO {
    private Integer id;
    private String movieTitle;
    private String genre;
    private String releaseYear;
    private String director;
    private String picture;
    private String trailer;
    private Integer franchise_id;
    private Set<Integer> characters;
}
