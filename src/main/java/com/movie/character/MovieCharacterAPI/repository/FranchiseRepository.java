package com.movie.character.MovieCharacterAPI.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.movie.character.MovieCharacterAPI.model.Franchise;

public interface FranchiseRepository extends JpaRepository<Franchise,Integer>{
    
}
