package com.movie.character.MovieCharacterAPI.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.movie.character.MovieCharacterAPI.model.Character;

public interface CharacterRepository extends JpaRepository<Character,Integer>{
    
}
