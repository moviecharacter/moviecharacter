package com.movie.character.MovieCharacterAPI.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.movie.character.MovieCharacterAPI.model.Movie;

public interface MovieRepository extends JpaRepository<Movie,Integer> {
    
}
