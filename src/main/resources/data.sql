create table IF NOT EXISTS character
(
    id        bigint not null
        primary key ,
    alias     varchar(50),
    full_name varchar(50),
    gender    varchar(10),
    picture   TEXT
);



create table IF NOT EXISTS franchise
(
    id          bigint not null
        primary key,
    description TEXT,
    name        varchar(50)
);



create table IF NOT EXISTS movie
(
    id           bigint not null
        primary key,
	movie_title VARCHAR(100),
    director     varchar(100),
    genre        TEXT,
    picture      TEXT,
    release_year varchar(4),
    trailer      TEXT,
    franchise_id bigint
);
INSERT INTO franchise(
	id, description, name)
	VALUES (1, 'Cinematic Universe of Marvel Studios', 'Marvel Studios');

INSERT INTO franchise(
	id, description, name)
	VALUES (2, 'DC Universe', 'DC');

INSERT INTO movie(
	id, director, genre, movie_title, picture, release_year, trailer, franchise_id)
	VALUES (1, 'Ryan Coogler', 'Action/Adventure', 'Black Panther', 'https://www.marvel.com/movies/black-panther', 2018, 'https://www.youtube.com/watch?v=xjDjIWPwcPU', 1);

INSERT INTO movie(
	id, director, genre, movie_title, picture, release_year, trailer, franchise_id)
	VALUES (2, 'Peyton Reed', 'Action/Adventure', 'Ant-Man and the Wasp: Quantumania', 'https://www.marvel.com/movies/antman-and-the-wasp-quantumania', 2023, 'https://www.youtube.com/watch?v=nUPABE2jBA8', 1);
INSERT INTO public.movie(
	id, director, genre, movie_title, picture, release_year, trailer, franchise_id)
	VALUES (3, 'Zack Snyder', 'Action/Adventure', 'Man of Steel', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRVShzCnP1BeOauROptr', 2013, 'https://www.youtube.com/watch?v=T6DJcgm3wNY', 2);

INSERT INTO public."character"(
	id, alias, full_name, gender, picture)
	VALUES (1, 'Clark Kent', 'Henry Cavill', 'Male', 'https://en.wikipedia.org/wiki/Henry_Cavill#/media/File:Henry_Cavill_(48417913146)_(cropped).jpg');

INSERT INTO public."character"(
	id, alias, full_name, gender, picture)
	VALUES (2, 'Antman', 'Paul Rudd', 'Male', 'https://www.imdb.com/name/nm0748620/mediaviewer/rm731323904/?ref_=nm_ov_ph');

INSERT INTO public."character"(
	id, alias, full_name, gender, picture)
	VALUES (3, 'Black Panther', 'Chadwick Boseman', 'Male', 'https://www.google.com/url?sa=i&url=http%3A%2F%2Ft0.gstatic.com%2Flicensed-image%3Fq%3Dtbn%3AANd9GcQsmeOXTpx-tLJ54xQ39D47VoTDERgxPM1KtNTDt31-i-j8cfXuQdg1bOlOwqBWTG__4VTiGcvdsxKvBms&psig=AOvVaw2G48LA-k8UOfs-oufz2Brs&ust=1675019565762000&source=images&cd=vfe&ved=0CA8QjRxqFwoTCLjrifH76vwCFQAAAAAdAAAAABAE');

INSERT INTO public.movie_character(
	character_id, movie_id)
	VALUES (1, 3);

INSERT INTO public.movie_character(
	character_id, movie_id)
	VALUES (2, 2);

INSERT INTO public.movie_character(
	character_id, movie_id)
	VALUES (3, 1);